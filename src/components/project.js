import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import "../App.css"
import { 
    Tabs, 
    Tab, 
    Grid, 
    Cell, 
    Card, 
    CardTitle, 
    CardText, 
    CardActions, 
    Button, 
    CardMenu, 
    IconButton 
} from 'react-mdl';

export default class project extends Component {
    constructor(props){
        super(props);
            this.state = { activeTab: 0};
    }

    toggleCategories(){
        if(this.state.activeTab === 0){
            return(
                <div className="card-button">
                <Card shadow={5} style={{maxWidth: '450', margin: 'auto'}}>
                    <CardTitle style={{
                        color: '#fff', 
                        height: '200px', 
                        background: 'url(/logo-og.png) center / cover'}}>
                        Event Management System
                        {/* <h1 className="project-name">UNIMAS Event Management System</h1> */}
                    </CardTitle>

                    <CardText>
                        This system is my university final year project. 
                        A system to help students to manage their event with the comprehensive feature to cater the problem of students facing during the planning process.
                    </CardText>
                    <CardActions border>
                        <Button coloured>
                            <a href="http://ems-unimas-system.herokuapp.com/" rel="noopener noreferrer" target="_blank"> System Link </a>
                        </Button>
                        <Button coloured>
                            <a href="/UsualManual.pptx" rel="noopener noreferrer" target="_blank" download> User Manual </a>
                        </Button>
                    </CardActions>
                    <CardMenu style={{color: '#fff'}}>
                        {/* <IconButton name="share" /> */}
                    </CardMenu>
                </Card>

                
                </div>
            )
        }else if(this.state.activeTab === 1){
            return(
                <div>
                    <h1>This is Angular</h1>
                </div>
            )
        }
    
    }

    toggleNextProject(){
        if(this.state.activeTab === 0){
            return(
                <div className="card-button">
                <Card shadow={5} style={{maxWidth: '450', margin: 'auto'}}>
                    <CardTitle style={{
                        color: '#fff', 
                        height: '200px', 
                        background: 'url(/spotify.png) center / cover'}}>
                        Spotify Clone 
                        {/* <h1 className="project-name">UNIMAS Event Management System</h1> */}
                    </CardTitle>

                    <CardText>
                        This spotify clone system is going to clone the user information of spotify and display the user information in UI.
                        Feature such as playing the song, checking library, and showing playlist will be update later.
                    </CardText>
                    <CardActions border>
                        <Button coloured>
                            <a href="https://spotify-clone-tch.netlify.app" rel="noopener noreferrer" target="_blank"> System Link </a>
                        </Button>
                        {/* <Button coloured>
                            <a href="/UsualManual.pptx" rel="noopener noreferrer" target="_blank" download> User Manual </a>
                        </Button> */}
                    </CardActions>
                    <CardMenu style={{color: '#fff'}}>
                        {/* <IconButton name="share" /> */}
                    </CardMenu>
                </Card>

                
                </div>
            )
        }else if(this.state.activeTab === 1){
            return(
                <div>
                    <h1>This is Angular</h1>
                </div>
            )
        }
    
    }

    toggleCovidProject(){
        if(this.state.activeTab === 0){
            return(
                <div className="card-button">
                <Card shadow={5} style={{maxWidth: '450', margin: 'auto'}}>
                    <CardTitle style={{
                        color: 'black', 
                        height: '200px', 
                        background: 'url(/covid-19.png) center / cover'}}>
                        COVID-19 Tracker System 
                        {/* <h1 className="project-name">UNIMAS Event Management System</h1> */}
                    </CardTitle>

                    <CardText>
                        Covid-19 track system, track by the country and world wide cases. Done by using React.JS, useEffect, useState, 
                        fetchd disease API. Libray dependency on Material UI, Chart.JS, and firebase.
                    </CardText>
                    <CardActions border>
                        <Button coloured>
                            <a href="https://covid-19-tch.netlify.app/" rel="noopener noreferrer" target="_blank"> System Link </a>
                        </Button>
                        <Button coloured>
                            <a href="/covid tracker.png" rel="noopener noreferrer" target="_blank" download> Wireframe </a>
                        </Button>
                    </CardActions>
                    <CardMenu style={{color: '#fff'}}>
                        {/* <IconButton name="share" /> */}
                    </CardMenu>
                </Card>

                
                </div>
            )
        }else if(this.state.activeTab === 1){
            return(
                <div>
                    <h1>This is Angular</h1>
                </div>
            )
        }
    }
    
    render() {
        return (
            <div className="category-tabs">
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
                    <Tab>React</Tab>
                    {/* <Tab>Angular</Tab> */}
                </Tabs>

                <section className="projects-main">
                    <div className="project-section">
                        <Grid className="projects-grid">
                            <Cell >
                                <div className="content">
                                    {this.toggleCategories()}
                                </div>
                            </Cell>
                        </Grid>

                        <Grid className="projects-grid">
                            <Cell >
                                <div className="content">
                                    {this.toggleNextProject()}
                                </div>
                            </Cell>
                        </Grid>

                    <Grid className="projects-grid">
                        <Cell col={12}>
                            <div className="content">
                                {this.toggleCovidProject()}
                            </div>
                        </Cell>
                    </Grid>
                    </div>
                    
                </section>
            </div>
        )
    }
}
