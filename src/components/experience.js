import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

export default class Experience extends Component {
    render() {
        return (
            <Grid>
            <Cell col={4}>
                <p>
                    {this.props.startYear} - {this.props.endYear}
                </p>
            </Cell>
            <Cell col={8}>
                <h4 style={{marginTop: '0px', textAlign: 'center'}}>{this.props.jobRole}</h4>
                <h5 style={{marginTop: '0px', fontSize: '15px', color: '#d6d6c2', textAlign: 'center'}}>{this.props.companyName}</h5>
                <p>
                    {this.props.jobDescription}
                </p>
            </Cell>
        </Grid>
        )
    }
}
