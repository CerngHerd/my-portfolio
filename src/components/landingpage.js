import React, { Component } from 'react'
import { Grid, Cell } from 'react-mdl'
import { FaLinkedin, FaLinkedinIn } from 'react-icons/fa';
import { AiOutlineLinkedin } from "react-icons/ai";
// <i class="fab fa-linkedin"></i>";

export default class landingpage extends Component {
    render() {
        return (
            <div style={{width: '100%', margin:'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img
                            src="/profileImg.jpg"
                            alt="avatar"
                            className="profile-picture"
                        />
                        <div className="banner-text">
                            <h1>
                                System Programmer 
                            </h1>
                            <hr/>
                            <p style={{color: "#ffffff"}}>
                            Hi, I’m Cerng Herd. 
                            Currently a fresh graduate student with a degree holder in the field of computer science and information technology.
                            I wish to become a product developer, experience designer, interaction, UI, UX or system programmer which encounter on current social trends.
                            I prefer to keep learning, continue challenging myself, and do interesting things that matter. My abundant energy fuels me in the pursuit of many interests, 
                            hobbies, areas of study and programming. I’m a fast learner, able to pick up new skills and juggle different projects and roles with relative ease.
                            </p>
                            <hr/>
                            <p>HTML/CSS | Bootstrap | JavaScript | React | Scala | API | PHP | JAVA | MySQL </p>
                            {/* <div className="social-links">
                                <AiOutlineLinkedin/>
                          
                            </div> */}
                        </div>
                    </Cell>
                    {/* <h3> Lets go for a <AiOutlineLinkedin />? </h3> */}
                </Grid>
            </div>
        )
    }
}
