import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

export default class Language extends Component {
    render() {
        return (
            <Grid>
            <Cell col={6}>
                {/* <h1 style= {{fontSize: '12px'}}>Language</h1> */}
                <p>
                    {this.props.language}
                </p>
            </Cell>
            <Cell col={3}>
                <h1 style= {{fontSize: '12px'}}>Spoken</h1>
                <p>{this.props.spoken}</p>
            </Cell>
            <Cell col={3}>
                <h1 style= {{fontSize: '12px'}}>written</h1>
                <p>{this.props.written}</p>
            </Cell>
        </Grid>
        )
    }
}
