import React, { Component } from 'react';
import { Grid, Cell, List, ListItem, ListItemContent } from 'react-mdl'

export default class contact extends Component {
    render() {
        // className="contact-body"
        // className="contact-grid"
        return (
            <div className="contact-body">
                <Grid className="contact-grid">
                    <Cell col={6}>
                        <h2>TEOH CERNG HERD</h2>
                        <img
                            src="/profileImg.jpg"
                            alt="avatar"
                            className="contact-picture"
                        />
                        <p style={{width: '75%', margin: 'auto', paddingTop: '1em'}}>
                        A fresh graduate student on bachelor degree of Computer Science and Information Technology.
                        Loves programming, writing, speaking, traveling, and lifting heavy things.
                        Experience in building a comprehensive website with an emphasis on backend web development. 
                        Common libraries of use: Axios, , react-bootstrap, react-router-dom, react-router, scala.
                        </p>
                    </Cell>

                    <Cell col={6}>
                        <h2>Contact Me</h2>
                        <hr/>

                        <div className="contact-list">
                        <List className="contact-way">
                            <ListItem>
                                <ListItemContent className="first-contact">
                                    <i className="fa fa-phone" aria-hidden="true"></i>
                                    <h7><br />+60-168322042</h7>
                                    
                                </ListItemContent>
                            </ListItem>
                            <ListItem>
                                <ListItemContent className="second-contact">
                                    <i className="fa fa-envelope" aria-hidden="true"></i>
                                    <h8><br />herd_1223@hotmail.com</h8>
                                </ListItemContent>
                            </ListItem>
                            <ListItem>
                                <ListItemContent className="third-contact">
                                    <a href="https://www.linkedin.com/in/teoh-cerng-herd/" style={{textDecoration: 'none', color: 'black'}}rel="noopener noreferrer" target="_blank">
                                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                    <a href="https://www.linkedin.com/in/teoh-cerng-herd/" style={{textDecoration: 'none', color: 'black'}}rel="noopener noreferrer" target="_blank">
                                    <h9><br />https://www.linkedin.com/in/teoh-cerng-herd/</h9>
                                    </a>
                                    
                                </ListItemContent>
                            </ListItem>
                        </List>
                        </div>
                        
                    </Cell>
                </Grid>
            </div>
        )
    }
}
