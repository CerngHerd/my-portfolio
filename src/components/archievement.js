import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

export default class Archievement extends Component {
    render() {
        return (
            <Grid>
            <Cell col={4}>
                <p>
                    {this.props.information}
                </p>
            </Cell>
            <Cell col={8}>
                <h4 style={{marginTop: '0px', fontSize: '15px'}}>{this.props.archievementTitle}</h4>
                <h5 style={{marginTop: '0px', fontSize: '15px'}}>{this.props.archievementInfo}</h5>
                <p>
                    {this.props.jobDescription}
                </p>
            </Cell>
        </Grid>
        )
    }
}
