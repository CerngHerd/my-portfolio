import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

export default class Education extends Component {
    render() {
        return (
            <Grid>
                <Cell col={4}>
                    <p>
                        {this.props.startYear} - {this.props.endYear}
                    </p>
                </Cell>
                <Cell col={8}>
                    <h4 className="core-course" style={{marginTop: '0px'}}>{this.props.schoolName}</h4>
                    <p>
                        {this.props.schoolDescription}
                    </p>
                    <h5 className="core-course" style={{fontSize: '25px', color: 'white'}}>{this.props.coreCourse}</h5>
                    <p>
                        {this.props.courseDesctiption}
                    </p>
                </Cell>
            </Grid>
        )
    }
}
