import React, { Component } from 'react';
import { Grid, Cell, Button } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Archievement from './archievement';
import Skill from './skill';

import Language from './language';

export default class resume extends Component {
    render() {
        return (
            <div className="resume-whole">
                <Grid>
                    <Cell col={4}>
                        <div style={{textAlign:'center'}}>
                            <img 
                                src="/profileImg.jpg"
                                alt="avatar"
                                className="profile-picture"
                                style={{height: '200px'}}
                            />
                        </div>

                        <h2 style={{paddingTop: '2em', fontSize: '40px'}}>
                            TEOH CERNG HERD
                        </h2>
                        <h4 style={{color: 'grey'}}>
                            System Programmer
                        </h4>
                        <hr style={{borderTop: '3px solid #833fb2', width:'50%'}}/>
                        <p>
                        A fresh Graduate student who are holding the Degree of Computer Science and Information Technology. 
                        Loves programming, writing, speaking, traveling, and lifting heavy things. Experience in building a 
                        comprehensive website with an emphasis on backend web development.  Programming Language skill on HTML/CSS, 
                        JavaScript, bootstrap, React.JS, Material UI, ChartJS, Scala Programming Language, Postman API testing, 
                        and MySQL database management. Excited to solve problems while being part of a transparent, diverse, 
                        hardworking and supportive team.
                        </p>
                        <hr style={{borderTop: '3px solid #833fb2', width:'50%'}}/>
                        <h5>Address</h5>
                        <p>
                            Taiping, Perak, Malaysia
                        </p>
                        <h5>Phone</h5>
                        <p>
                            +60-168322042
                        </p>
                        <h5>Email</h5>
                        <p>
                            herd_1223@hotmail.com
                        </p>
                        <hr style={{borderTop: '3px solid #833fb2', width:'50%'}}/>

                        <Button coloured  style={{border: '3px solid #ccccb3', background: '#cbcbb3', hover: 'yellow'}}>
                            <a href="/My-Resume-TCH.pdf" style={{background: '#c1c1a4', color: 'black', textDecoration: 'none'}} rel="noopener noreferrer" target="_blank" download> Download My Resume </a>
                        </Button>
                    </Cell>
                    <Cell className="resume-right-col" col={8}>
                        <h2>Education</h2>

                            <Education 
                                startYear={2016}
                                endYear={2020}
                                // level="Bachelor's Degree in Computer Science/Information Technology  |  Malaysia"
                                schoolName="UNIVERSITY MALAYSIA SARAWAK"
                                schoolDescription= {<>- Bachelor's Degree in Computer Science and Information Technology 
                                 <br /> - Majoring in Multimedia Computing</>}
                                 coreCourse={"Core Course"}
                                 courseDesctiption={
                                     <div className="course-description"> 
                                        <div className="left-course">
                                        - System Analysis and Design <br />
                                        - Computer Architecture <br />
                                        - Data Mining <br />
                                        - UI/UX Design <br />
                                        - Data Visualisation <br />
                                        - Human Computer Interaction <br />
                                        - Computer Graphics <br />
                                        - Computer Security <br />
                                        </div>
                                        
                                        <div className="right-course">
                                        - Intelligent Systems <br />
                                        - Database Concept and Design <br />
                                        - Java for Multimedia Programming <br />
                                        - Communication and Computer Network <br />
                                        - Object Oriented Software Development <br />
                                        - Web Based System Development <br />
                                        - Computer Game Design & Development <br />
                                        - Computer System Administration and <br /> Management <br />

                                        </div>
                                     </div>
                                 }
                            />

                            <hr style={{borderTop: '3px solid white'}}/>

                            <h2>Experience</h2>

                            <Experience
                                startYear="April"
                                endYear="July, 2016"
                                companyName="Celcom Sales Executive  |  Perak, Malaysia"
                                jobRole="Communication Assistant"
                                jobDescription={<>- Develop and manage relationship with existing customers.  
                                <br /> - Communicated with customers and provided assistance.
                                <br /> - Explained the features of product to customers.
                                <br /> - Composed, revised and proofread corporate communications.
                                <br /> - Highly experienced in managing the computer system of the company.
                                <br /> - Entertained with almost more than 1000+ customer with a proficiently communication skill.</>}
                            />

                            <Experience
                                startYear="January"
                                endYear="August, 2019"
                                companyName="NiuAce  |  Selangor, Malaysia"
                                jobRole="Internship for Backend Web Developer"
                                jobDescription={<>- Collaborating with the front-end developers to establish objectives and design more functional, cohesive codes to enhance the user experience.  
                                    <br /> - Develop new products or new features of existing product.
                                    <br /> - Maintain existing products to keep product in working order.
                                    <br /> - Manage individual project priorities, deadlines and deliverable​.
                                    <br /> - Highly experienced in managing the computer system of the company.
                                    <br /> - Generate Comprehensive Documentation.
                                    <br /> - Conduct Backend API test and optimize the performance.</>}
                            />

                            <hr style={{borderTop: '3px solid white'}}/>

                            <h2>Archievement</h2>

                            <Archievement 
                                information="Ideathon Series 2018"
                                archievementTitle=" - Develop an idea with using IoT to in E- disaster management"
                                // archievementInfo="Participating the club activities."
                            />

                            <Archievement 
                                information="Cloud Study Jam"
                                archievementTitle=" - Access and complete all the quest"
                                // archievementInfo="Participating the club activities."
                            />

                            <Archievement 
                                information="Developer Student Club"
                                archievementTitle="- Speaker of Cloud Study Jam"
                                // archievementInfo="Participating the club activities."
                            />

                            <hr style={{borderTop: '3px solid white'}}/>

                            <h2>Skills</h2>

                            <Skill
                                skill="HTML/CSS"
                                progress="75"
                            />

                            <Skill
                                skill="JavaScript"
                                progress="70"
                            />

                            <Skill
                                skill="Bootstrap"
                                progress="65"
                            />
                            
                            <Skill
                                skill="React"
                                progress="80"
                            />
                            
                            <Skill
                                skill="SASS"
                                progress="65"
                            />
                            
                            <Skill
                                skill="Scala"
                                progress="85"
                            />

                            <Skill
                                skill="JAVA "
                                progress="70"
                            />
                            
                            <Skill
                                skill="API"
                                progress="80"
                            />
  
                            <Skill
                                skill="PHP"
                                progress="40"
                            />

                            <Skill
                                skill="MySQL"
                                progress="75"
                            />

                            <hr style={{borderTop: '3px solid white'}}/>

                            <h2>Language</h2>
                            <Grid>
                            <Cell col={6}>
                                <h1 style= {{fontSize: '12px'}}>Language</h1>
                                <p>English</p>
                                <p>Chinese</p>
                                <p>Malay</p>
                            </Cell>
                            <Cell col={3}>
                                <h1 style= {{fontSize: '12px'}}>Spoken</h1>
                                <p>8</p>
                                <p>10</p>
                                <p>7</p>
                            </Cell>
                            <Cell col={3}>
                                <h1 style= {{fontSize: '12px'}}>written</h1>
                                <p>8</p>
                                <p>10</p>
                                <p>7</p>
                            </Cell>
                            </Grid>      
                    </Cell>
                </Grid>
            </div>
        )
    }
}
